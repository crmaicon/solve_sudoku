
cell =[]

def InsertIni():
        for i in range(9):
                cell.append([])
                for ii in range(9):
                        cell[i].append([0,[]])

        cell[0][1] = [6,[]]
        cell[0][4] = [8,[]]
        cell[0][6] = [4,[]]
        cell[0][7] = [2,[]]

        "Linha 2"
        cell[1][1] = [1,[]]
        cell[1][2] = [5,[]]
        cell[1][4] = [6,[]]
        cell[1][6] = [3,[]]
        cell[1][7] = [7,[]]
        cell[1][8] = [8,[]]

        "Linha 3"
        cell[2][3] = [4,[]]
        cell[2][7] = [6,[]]

        "Linha 4"
        cell[3][0] = [1,[]]
        cell[3][3] = [6,[]]
        cell[3][5] = [4,[]]
        cell[3][6] = [8,[]]
        cell[3][7] = [3,[]]

        "Linha 5"
        cell[4][0] = [3,[]]
        cell[4][2] = [6,[]]
        cell[4][4] = [1,[]]
        cell[4][6] = [7,[]]
        cell[4][8] = [5,[]]

        "Linha 6"
        cell[5][1] = [8,[]]
        cell[5][3] = [3,[]]
        cell[5][4] = [5,[]]

        "Linha 7"
        cell[6][0] = [8,[]]
        cell[6][1] = [3,[]]
        cell[6][3] = [9,[]]
        cell[6][4] = [4,[]]

        "Linha 8"
        cell[7][1] = [7,[]]
        cell[7][2] = [2,[]]
        cell[7][3] = [1,[]]
        cell[7][4] = [3,[]]
        cell[7][6] = [9,[]]

        "Linha 9"
        cell[8][2] = [9,[]]
        cell[8][4] = [2,[]]
        cell[8][6] = [6,[]]
        cell[8][7] = [1,[]]

def SearchLine(line, number):        
        for i in range(9):
                if (cell[line][i][0] == number):
                        return True
        return False

def SearchColunm(colunm, number):        
        for i in range(9):
                if (cell[i][colunm][0] == number):
                        return True
        return False

def SearchBlock(line, colunm, number):
        firstLine = 0
        firstColunm = 0
        if(line > 5):
                firstLine = 6
        elif (line > 2):
                firstLine = 3
        
        if(colunm > 5):
                firstColunm = 6
        elif (colunm > 2):
                firstColunm = 3

        for i in range(firstLine, firstLine+3 ,1):
                for ii in range(firstColunm, firstColunm+3, 1):
                        if (cell[i][ii][0] == number):
                                return True
        return False


def SearchEquals():
        for i in range(9):
                for ii in range(9):
                        if (cell[i][ii][0] == 0):
                                for number in range(1,10,1):
                                        if (not SearchLine(i,number) and not SearchColunm(ii, number) and not SearchBlock(i, ii, number)):
                                                cell[i][ii][1].append(number)

def InsertLine():
        modified =False
        for i in range(9):
                for ii in range(9):
                        if (cell[i][ii][0] == 0):
                                if (len(cell[i][ii][1]) == 1):
                                        cell[i][ii][0] = cell[i][ii][1][0]                                        
                                        modified = True
        return modified

def ClearList():
        for i in range(9):
                for ii in range(9):
                        cell[i][ii][1] = []

InsertIni()
SearchEquals()

for iten in cell:
        print(f'{iten[0][0]} - {iten[1][0]} - {iten[2][0]} - {iten[3][0]} - {iten[4][0]} - {iten[5][0]} - {iten[6][0]} - {iten[7][0]} - {iten[8][0]}')

for i in range(9):
        for ii in range(9):
                print(f'{i}-{ii} - {cell[i][ii]}')

print('----------------------------')

while (InsertLine()):
        ClearList()
        SearchEquals()

for iten in cell:
        print(f'{iten[0][0]} - {iten[1][0]} - {iten[2][0]} - {iten[3][0]} - {iten[4][0]} - {iten[5][0]} - {iten[6][0]} - {iten[7][0]} - {iten[8][0]}')